""" Кольцевой буфер FIFO на основе зацикленного связного списка """

class LinkedNode:

    def __init__(self, data=None, nref=None):
        self.__data = data
        self.__nref = nref

    def set_data(self, data):
        self.__data = data

    def get_data(self):
        return self.__data

    def set_nref(self, nref):
        self.__nref = nref

    def get_nref(self):
        return self.__nref


class CircularBufferTwo:

    def __init__(self, buff_len=10):

        self.__buff_len = buff_len

        start_node, prev_node = LinkedNode(), LinkedNode()
        prev_node.set_nref(start_node)
        next_node = prev_node

        for i in range(self.__buff_len - 2):
            prev_node = LinkedNode()
            prev_node.set_nref(next_node)
            next_node = prev_node

        start_node.set_nref(next_node)

        self.__tail_node = start_node
        self.__head_node = start_node
        self.__times_overfilled = 0

    def __str__(self):

        out = "[..., " + str(self.__tail_node.get_data()) + ", "
        curr_node = self.__tail_node.get_nref()

        while curr_node != self.__tail_node:
            out += str(curr_node.get_data()) + ", "
            curr_node = curr_node.get_nref()

        out += "...]"

        return out

    def push(self, obj):

        curr_value = self.__head_node.get_data()
        if curr_value is not None:
            self.__times_overfilled += 1

        self.__head_node.set_data(obj)
        self.__head_node = self.__head_node.get_nref()

    def pop(self):

        while self.__times_overfilled > 0:
            self.__tail_node = self.__tail_node.get_nref()
            self.__times_overfilled -= 1

        out = self.__tail_node.get_data()
        self.__tail_node.set_data(None)
        self.__tail_node = self.__tail_node.get_nref()
        return out


if __name__ == "__main__":

    print("---Circular Buffer Two---")
    buff_2 = CircularBufferTwo(7)
    print("\nInitial condotion: %s" % buff_2)

    print("\n Overload test:")
    for i in range(10):
        buff_2.push(i)
        print(buff_2)

    for i in range(3):
        buff_2.pop()
        print(buff_2)

    for i in range(4):
        buff_2.push(i)
        print(buff_2)