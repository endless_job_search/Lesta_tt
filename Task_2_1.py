""" Кольцевой буфер FIFO на основе списка """

class CircularBufferOne:

    def __init__(self, buff_len=10):

        self.__buff_len = buff_len
        self.__buff = [None] * self.__buff_len

        self.__tail = 0
        self.__head = 0
        self.__times_overfilled = 0

    def __str__(self):

        out = "[..., "
        for i in range(self.__buff_len):
            out += str(self.__buff[i]) + ", "
        out += "...]"

        return out

    def __increment(self, iterator):

        if iterator < self.__buff_len - 1:
            return iterator + 1
        else:
            return 0

    def push(self, obj):

        if self.__buff[self.__head] is not None:
            self.__times_overfilled = self.__increment(self.__times_overfilled)

        self.__buff[self.__head] = obj
        self.__head = self.__increment(self.__head)

    def pop(self):

        while self.__times_overfilled > 0:
            self.__tail = self.__increment(self.__tail)
            self.__times_overfilled -= 1

        out = self.__buff[self.__tail]
        self.__buff[self.__tail] = None
        self.__tail = self.__increment(self.__tail)
        return out


if __name__ == "__main__":

    print("---Circular Buffer One---")
    buff_1 = CircularBufferOne(7)
    print("\nInitial condotion: %s" % buff_1)

    print("\n1-st test:")
    for i in range(3):
        buff_1.push(i)
        print(buff_1)

    for i in range(3):
        buff_1.pop()
        print(buff_1)

    print("\n2-nd test:")
    for i in range(10):
        buff_1.push(i)
        print(buff_1)

    for i in range(3):
        buff_1.pop()
        print(buff_1)

    for i in range(4):
        buff_1.push(i)
        print(buff_1)