import random
from time import perf_counter

def quicksort(nums):

    ''' Задача: реализовать сортировку, которая быстрее всего отсортирует данный ей массив чисел.
        Массив может быть любого размера со случайным порядком чисел.

        По результатам анализа статьи:
        http://www.mal-bioit.ru/speed-of-sorting#summarizing_charts
        (и других аналогичных) решил использовать быструю сортировку Хоара,
        сложность которой в худшем случае - O(n^2),среднем и лучшем - O(n * log(n))

        В исследовании как раз показана зависимость веремени сортировки случайных массивов от их длины.
        И быстрая сортировка Хоара показывает наилушие результаты'''
    
    if len(nums) <= 1:
        return nums
    else:
        q = random.choice(nums)
        
    l_nums = [n for n in nums if n < q]
    e_nums = [q] * nums.count(q)
    b_nums = [n for n in nums if n > q]
 
    return quicksort(l_nums) + e_nums + quicksort(b_nums)


if __name__ == "__main__":

    random.seed(42)
    len_arr = 1000
    test_arr = [random.randint(0, 1e5) for i in range(len_arr)]
    
    start_time = perf_counter()
    sorted_arr = quicksort(test_arr)
    #print(test_arr, '\n', sorted_arr)

    print('Sorting an array of {} elements takes {} sec'.format(len_arr, round(perf_counter() - start_time, 5)))
